﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private PlayerController m_Player;
    
    [SerializeField] private Vector3 m_PositionOffset;
    [SerializeField] private float m_MaxZoom = 25;
    [SerializeField] private float m_MinZoom = 100;
    [SerializeField] private float m_ZoomSpeed = 10000;

    private Vector3 m_CurrentPositionOffset;

    private void Start()
    {
        m_CurrentPositionOffset = m_PositionOffset;
    }

    private void Update()
    {
        m_CurrentPositionOffset.y -= Input.GetAxis("Mouse ScrollWheel") * m_ZoomSpeed;
        m_CurrentPositionOffset.y = Mathf.Clamp(m_CurrentPositionOffset.y, m_MaxZoom, m_MinZoom);
    }

    private void LateUpdate()
    {
        Vector3 pos = m_Player.transform.position + m_CurrentPositionOffset;
        transform.position = pos;
        transform.LookAt(m_Player.transform);
    }
}