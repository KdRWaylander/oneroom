﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITogglePanel : MonoBehaviour
{
    [SerializeField] private KeyCode m_ToggleKeyCode;
    [SerializeField] private GameObject m_Panel;
    [SerializeField] private bool m_InitialState = true;

    private void Start()
    {
        m_Panel.SetActive(m_InitialState);
    }

    private void Update()
    {
        if (Input.GetKeyDown(m_ToggleKeyCode))
        {
            m_Panel.SetActive(!m_Panel.activeSelf);
        }
    }
}