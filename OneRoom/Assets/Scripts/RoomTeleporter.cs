﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomTeleporter : MonoBehaviour
{
    [SerializeField] private Direction m_Direction;
    [SerializeField] private Vector2 m_TeleportAmount;

    private Room m_ParentRoom;
    private Vector2 m_TeleportDirection;

    private void Start()
    {
        m_ParentRoom = GetComponentInParent<Room>();
        
        switch (m_Direction)
        {
            case Direction.North:
                m_TeleportDirection = Vector2.up;
                break;
            case Direction.East:
                m_TeleportDirection = Vector2.right;
                break;
            case Direction.South:
                m_TeleportDirection = Vector2.down;
                break;
            case Direction.West:
                m_TeleportDirection = Vector2.left;
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            // Switch off nav mesh agent to allow teleportation
            NavMeshAgent agent = other.GetComponent<NavMeshAgent>();
            agent.ResetPath();
            agent.enabled = false;
            
            Vector2 teleportation = m_TeleportDirection * m_TeleportAmount;
            Vector3 destination = other.transform.position + new Vector3(teleportation.x, 0, teleportation.y);
            
            other.transform.position = destination;

            // Handle visibility and light
            m_ParentRoom.SetVisibility(false);
            Room nextRoom = RoomsManager.Instance.GetRoom(m_ParentRoom.RoomIndex + m_TeleportDirection);
            nextRoom.SetVisibility(true);
            other.GetComponent<Player>().TurnFlashLight(!nextRoom.IsPowered);

            // Switch agent back on
            agent.enabled = true;
        }
    }
}

    [System.Serializable]
public enum Direction
{
    North,
    East,
    South,
    West
}