﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsManager : MonoBehaviour
{
    public static RoomsManager Instance { get; private set; }
    public Room CurrentActiveRoom { get; private set; }

    [SerializeField] private Vector2 m_StartRoomIndex = Vector2.zero;
    [SerializeField] private Room[] m_Rooms;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        foreach(Room r in m_Rooms)
        {
            r.SetVisibility(false);
            
            if (r.RoomIndex == m_StartRoomIndex)
            {
                CurrentActiveRoom = r;
                r.SetVisibility(true);
            }
        }

        GetRoom(new Vector2(1, 4)).SwitchPower(true);
    }

    public Room GetRoom(Vector2 roomIndex)
    {
        foreach(Room r in m_Rooms)
        {
            if (r.RoomIndex == roomIndex)
                return r;
        }

        // if none found, return current active room
        return CurrentActiveRoom;
    }

    public Room GetRoomWithPos(Vector3 pos)
    {
        return GetRoom(new Vector2(pos.x % 100, pos.z % 80));
    }
}