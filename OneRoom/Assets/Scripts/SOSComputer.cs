﻿using UnityEngine;

public class SOSComputer : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 6f;

    public void InteractWith(Player player)
    {
        Debug.Log(FindObjectOfType<Crater>().IsSatelliteReady);
        Debug.Log(GetComponentInParent<Room>().IsPowered);
        
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance
            && FindObjectOfType<Crater>().IsSatelliteReady
            && GetComponentInParent<Room>().IsPowered)
        {
            ObjectivesManager.Instance.AchieveSOS();
        }
    }
}