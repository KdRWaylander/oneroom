﻿using UnityEngine;

public class Generator : MonoBehaviour, ICollectible
{
    public GameObject GameObject { get { return gameObject; } }

    public float CollectionDistance { get { return m_CollectionDistance; } }
    [SerializeField] private float m_CollectionDistance = 12f;

    public bool IsStackable { get { return false; } }

    public int MaxStackQuantity { get { return 1; } }

    public Sprite Sprite { get { return m_Sprite; } }
    [SerializeField] private Sprite m_Sprite;

    public void BeCollectedBy(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < CollectionDistance)
        {
            player.TryEquipCollectible(this);
        }
    }
}