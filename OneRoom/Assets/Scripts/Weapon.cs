﻿using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour, ICollectible, IUsable
{
    public float CollectionDistance { get { return m_CollectionDistance; } }
    [SerializeField] private float m_CollectionDistance = 6f;

    [SerializeField] private Transform m_FirePoint;

    public int AmmoCount { get { return m_AmmoCount; } }

    public bool IsStackable { get { return false; } }

    public int MaxStackQuantity { get { return 1; } }

    public GameObject GameObject { get { return gameObject; } }

    [SerializeField] private int m_AmmoCount = 7;

    [SerializeField] private AudioClip m_WeaponClip;
    [SerializeField] private AudioClip m_ClickClip;
    private AudioSource m_AudioSource;

    public Sprite Sprite { get { return m_Sprite; } }
    [SerializeField] private Sprite m_Sprite;

    public event Action<Weapon> NewBulletFired;
    public event Action<Weapon> NewAmmoToClip;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    public void BeCollectedBy(Player player)
    {
        if(Vector3.Distance(player.transform.position, transform.position) < CollectionDistance)
        {
            player.TryEquipCollectible(this);
        }
    }

    public void Use()
    {
        if(AmmoCount <= 0)
        {
            m_AudioSource.PlayOneShot(m_ClickClip);
            return;
        }

        m_AmmoCount -= 1;
        m_AudioSource.PlayOneShot(m_WeaponClip);

        HandleNewBulletFired(this);
        
        Ray ray = new Ray(m_FirePoint.position, -m_FirePoint.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit) && hit.transform.GetComponent<ITakeDamage>() != null)
        {
            hit.transform.GetComponent<ITakeDamage>().TakeDamage();
        } 
    }

    public void AddAmmo(int ammoCount)
    {
        m_AmmoCount += ammoCount;
        HandleNewAmmoToClip(this);
    }

    public void WithdrawAmmo(int ammoCount)
    {
        m_AmmoCount -= ammoCount;
    }

    private void HandleNewBulletFired(Weapon weapon)
    {
        if (NewBulletFired != null)
            NewBulletFired(weapon);
    }

    private void HandleNewAmmoToClip(Weapon weapon)
    {
        if (NewBulletFired != null)
            NewBulletFired(weapon);
    }
}