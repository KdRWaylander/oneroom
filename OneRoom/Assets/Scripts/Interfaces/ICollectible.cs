﻿using System.Transactions;
using UnityEngine;

public interface ICollectible
{
    GameObject GameObject { get; }
    float CollectionDistance { get; }
    bool IsStackable { get; }
    int MaxStackQuantity { get; }
    Sprite Sprite { get; }

    void BeCollectedBy(Player player);
}