﻿public interface IInteractible
{
    float InteractionDistance { get; }

    void InteractWith(Player player);
}