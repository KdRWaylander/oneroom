﻿using UnityEngine;

public class Room : MonoBehaviour
{
    public Vector2 RoomIndex { get { return new Vector2(transform.position.x / 100, transform.position.z / 80); } }

    [SerializeField] private LayerMask m_VisibleLayer;
    [SerializeField] private LayerMask m_InvisibleLayer;

    [SerializeField] private Transform m_OffLightsHolderTransform;
    [SerializeField] private Transform m_OnLightsHolderTransform;

    public Transform PropsHolder { get { return m_PropsHolder; } }
    [SerializeField] private Transform m_PropsHolder;

    public bool IsPowered { get; private set; }

    public void SetVisibility(bool visibility)
    {
        if (visibility)
            ChangeObjectAndChildrenLayer(gameObject, (int)Mathf.Log(m_VisibleLayer.value, 2));
        else
            ChangeObjectAndChildrenLayer(gameObject, (int)Mathf.Log(m_InvisibleLayer.value, 2));
    }

    private void ChangeObjectAndChildrenLayer(GameObject go, int layer)
    {
        go.layer = layer;

        for (int i = 0; i < go.transform.childCount; i++)
        {
            ChangeObjectAndChildrenLayer(go.transform.GetChild(i).gameObject, layer);
        }
    }

    public void SwitchPower(bool status)
    {
        IsPowered = status;
        TurnLights(status);
    }

    private void TurnLights(bool status)
    {
        if(status)
        {
            m_OnLightsHolderTransform.gameObject.SetActive(true);
            m_OffLightsHolderTransform.gameObject.SetActive(false);
        }
        else
        {
            m_OnLightsHolderTransform.gameObject.SetActive(false);
            m_OffLightsHolderTransform.gameObject.SetActive(true);
        }
    }
}

[System.Serializable]
public enum Connections
{
    N,
    E,
    S,
    O,
    NE,
    NS,
    NO,
    ES,
    EO,
    SO,
    NES,
    NEO,
    NSO,
    ESO,
    NESO
}