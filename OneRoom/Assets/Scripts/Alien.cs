﻿using UnityEngine;

public class Alien : MonoBehaviour, ITakeDamage
{
    public void TakeDamage()
    {
        Destroy(gameObject);
    }
}