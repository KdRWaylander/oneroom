﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainMenuCanvas : MonoBehaviour
{
    [SerializeField] private GameObject YesButton;
    [SerializeField] private GameObject NoButton;

    private void Start()
    {
        YesButton.SetActive(false);
        NoButton.SetActive(false);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        YesButton.SetActive(true);
        NoButton.SetActive(true);
    }

    public void ConfirmQuit()
    {
        Application.Quit();
    }

    public void CancelQuit()
    {
        YesButton.SetActive(false);
        NoButton.SetActive(false);
    }
}