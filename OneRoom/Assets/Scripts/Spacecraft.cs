﻿using UnityEngine;

public class Spacecraft : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 15f;

    public void InteractWith(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance && player.GetComponentInChildren<ICollectible>() != null)
        {
            if (player.GetComponentInChildren<ICollectible>().GameObject.transform.tag == "Roomba" && ObjectivesManager.Instance.RoombaAchieved == false)
            {
                ICollectible collectible = player.GetComponentInChildren<ICollectible>();
                FindObjectOfType<Inventory>().ResetCollectibleImage(collectible);

                Destroy(collectible.GameObject);

                ObjectivesManager.Instance.AchieveRoomba();
            }
            else if (player.GetComponentInChildren<ICollectible>().GameObject.transform.tag == "Oil" && ObjectivesManager.Instance.OilAchieved == false)
            {
                ICollectible collectible = player.GetComponentInChildren<ICollectible>();
                FindObjectOfType<Inventory>().ResetCollectibleImage(collectible);

                Destroy(collectible.GameObject);

                ObjectivesManager.Instance.AchieveOil();
            }
        }
    }
}