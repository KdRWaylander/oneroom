﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Transform m_RightArm;
    [SerializeField] private Transform m_LeftArm;
    [SerializeField] private Transform m_FlashLight;

    private Inventory m_InventoryCanvas;
    private bool m_HasWeapon;

    private void Awake()
    {
        m_InventoryCanvas = GetComponentInChildren<Inventory>();
    }

    private void Start()
    {
        m_HasWeapon = false;
    }

    private void Update()
    {
        if (GameManager.Instance.GameState != GameState.Play)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                if (hit.transform.GetComponent<ICollectible>() != null)
                {
                    hit.transform.GetComponent<ICollectible>().BeCollectedBy(this);
                }

                if (hit.transform.GetComponent<IInteractible>() != null)
                    hit.transform.GetComponent<IInteractible>().InteractWith(this);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && GetComponentInChildren<IUsable>() != null)
        {
            GetComponentInChildren<IUsable>().Use();
        }

        if(Input.GetKeyDown(KeyCode.A) && GetComponentInChildren<ICollectible>() != null)
        {
            DropCollectible(GetComponentInChildren<ICollectible>());
        }
    }

    public void TurnFlashLight(bool status)
    {
        m_FlashLight.gameObject.SetActive(status);
        m_LeftArm.localRotation = (status) ? Quaternion.Euler(new Vector3(60, 0, 0)) : Quaternion.identity;
    }

    public void TryEquipCollectible(ICollectible collectible)
    {
        string tag = collectible.GameObject.transform.tag;

        if (tag == "Weapon")
        {
            Weapon weapon = collectible.GameObject.GetComponent<Weapon>();
            if (m_HasWeapon)
            {
                // Takes the bullets in the the gun but does nothing with the gun (= leaves it on the floor)
                GetComponentInChildren<Weapon>().AddAmmo(weapon.AmmoCount);
                weapon.WithdrawAmmo(weapon.AmmoCount);
            }
            else
            {
                weapon.transform.parent = m_RightArm;
                weapon.transform.localPosition = new Vector3(-.64f, -2f, -.64f);
                weapon.transform.localRotation = Quaternion.identity;
                m_RightArm.localRotation = Quaternion.Euler(90, 0, 0);
                m_HasWeapon = true;

                m_InventoryCanvas.SetCollectibleImage(collectible);
            }
        }
        else
        {
            if(GetComponentInChildren<ICollectible>() != null) // Aleady a collectible on the player
                DropCollectible(GetComponentInChildren<ICollectible>());
            
            if (tag == "Generator" )//&& RoomsManager.Instance.GetRoomWithPlayerPos(transform.position).IsPowered)
            {
                collectible.GameObject.GetComponentInParent<Room>().SwitchPower(false);
                TurnFlashLight(true);
            }

            collectible.GameObject.transform.parent = transform;
            collectible.GameObject.transform.localPosition = 10 * Vector3.down;
            collectible.GameObject.transform.localRotation = Quaternion.identity;
            m_InventoryCanvas.SetCollectibleImage(collectible);
        }
    }

    public void DropCollectible(ICollectible collectible)
    {
        collectible.GameObject.transform.position = transform.position;
        collectible.GameObject.transform.rotation = transform.rotation;
        collectible.GameObject.transform.parent = RoomsManager.Instance.GetRoomWithPos(transform.position).PropsHolder;

        if(collectible.GameObject.transform.tag == "Weapon")
        {
            collectible.GameObject.transform.rotation = Quaternion.Euler(new Vector3(180, Random.Range(0, 359), -90));
            m_RightArm.localRotation = Quaternion.identity;
            m_HasWeapon = false;
        }

        m_InventoryCanvas.ResetCollectibleImage(collectible);
    }
}