﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameCanvas : MonoBehaviour
{
    [SerializeField] private GameObject YesButton;
    [SerializeField] private GameObject NoButton;

    private void Start()
    {
        YesButton.SetActive(false);
        NoButton.SetActive(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        YesButton.SetActive(true);
        NoButton.SetActive(true);
    }

    public void ConfirmQuit()
    {
        Application.Quit();
    }

    public void CancelQuit()
    {
        YesButton.SetActive(false);
        NoButton.SetActive(false);
    }
}