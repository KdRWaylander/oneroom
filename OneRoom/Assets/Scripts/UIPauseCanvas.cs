﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPauseCanvas : MonoBehaviour
{
    [SerializeField] private GameObject m_PausePanel;

    private void Start()
    {
        m_PausePanel.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.Instance.GameState == GameState.Play)
            {
                GameManager.Instance.PauseGame();
                m_PausePanel.SetActive(true);
            }
            else if (GameManager.Instance.GameState == GameState.Pause)
            {
                ResumeGame();
            }
        }
    }

    public void ResumeGame()
    {
        GameManager.Instance.ResumeGame();
        m_PausePanel.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}