﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Image m_CollectibleImage;
    [SerializeField] private TextMeshProUGUI m_SecondaryText;

    public void SetCollectibleImage(ICollectible collectible)
    {
        m_CollectibleImage.sprite = collectible.Sprite;
        m_SecondaryText.text = "";

        if(collectible.GameObject.transform.tag == "Weapon")
        {
            Weapon weapon = collectible.GameObject.transform.GetComponent<Weapon>();
            m_SecondaryText.text = "Balles restantes: " + weapon.AmmoCount.ToString();
            weapon.NewBulletFired += UpdateRemainingAmmoText;
            weapon.NewAmmoToClip += UpdateRemainingAmmoText;
        }
    }

    public void ResetCollectibleImage(ICollectible collectible)
    {
        m_CollectibleImage.sprite = null;
        m_SecondaryText.text = "";

        if (collectible.GameObject.transform.tag == "Weapon")
        {
            Weapon weapon = collectible.GameObject.transform.GetComponent<Weapon>();
            weapon.NewBulletFired -= UpdateRemainingAmmoText;
            weapon.NewAmmoToClip -= UpdateRemainingAmmoText;
        }
    }

    private void UpdateRemainingAmmoText(Weapon weapon)
    {
        m_SecondaryText.text = "Balles restantes: " + weapon.AmmoCount.ToString();
    }
}