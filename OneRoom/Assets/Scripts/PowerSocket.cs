﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSocket : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 12f;

    private Vector3 m_LocalPositionOffset = new Vector3(2.5f, .4f, 7.35f);
    private Vector3 m_EuleurLocalRotationOffset = 90 * Vector3.up;

    public void InteractWith(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance
            && player.GetComponentInChildren<ICollectible>() != null
            && player.GetComponentInChildren<ICollectible>().GameObject.transform.tag == "Generator")
        {
            ICollectible collectible = player.GetComponentInChildren<ICollectible>();
            FindObjectOfType<Inventory>().ResetCollectibleImage(collectible);

            collectible.GameObject.transform.parent = transform;
            collectible.GameObject.transform.localPosition = m_LocalPositionOffset;
            collectible.GameObject.transform.localRotation = Quaternion.Euler(m_EuleurLocalRotationOffset);

            GetComponentInParent<Room>().SwitchPower(true);
            player.TurnFlashLight(false);
        }
    }
}