﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameState GameState { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        GameState = GameState.Play;
        Time.timeScale = 1;
    }

    public void PauseGame()
    {
        GameState = GameState.Pause;
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        GameState = GameState.Play;
        Time.timeScale = 1;
    }

    public void Victory()
    {
        GameState = GameState.Victory;
        Time.timeScale = 0;
        SceneManager.LoadScene(2);
    }

    public void Defeat()
    {
        GameState = GameState.Defeat;
        Time.timeScale = 0;
        SceneManager.LoadScene(3);
    }
}

[System.Serializable]
public enum GameState
{
    Play,
    Pause,
    Victory,
    Defeat
}