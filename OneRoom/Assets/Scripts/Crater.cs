﻿using System.Security.Cryptography;
using UnityEngine;

public class Crater : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 6f;
    [SerializeField] private Vector3 m_LocalPositionOffset = new Vector3(7, 0, 4.6f);
    [SerializeField] private Vector3 m_EulerLocalRotationOffset = new Vector3(0, 106, 0);

    public bool IsSatelliteReady { get; private set; }

    private void Start()
    {
        IsSatelliteReady = false;
    }

    public void InteractWith(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance
            && player.GetComponentInChildren<ICollectible>() != null
            && player.GetComponentInChildren<ICollectible>().GameObject.transform.tag == "Satellite")
        {
            Destroy(GetComponent<MeshRenderer>());
            Destroy(GetComponent<MeshFilter>());
            Destroy(GetComponent<MeshCollider>());
            
            ICollectible collectible = player.GetComponentInChildren<ICollectible>();
            FindObjectOfType<Inventory>().ResetCollectibleImage(collectible);

            collectible.GameObject.transform.parent = transform;
            collectible.GameObject.transform.localPosition = m_LocalPositionOffset;
            collectible.GameObject.transform.localRotation = Quaternion.Euler(m_EulerLocalRotationOffset);

            Destroy(collectible.GameObject.transform.GetComponent<Satellite>());

            IsSatelliteReady = true;
        }
    }
}