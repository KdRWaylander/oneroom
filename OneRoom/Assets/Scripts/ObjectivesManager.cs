﻿using UnityEngine;
using UnityEngine.UI;

public class ObjectivesManager : MonoBehaviour
{
    public static ObjectivesManager Instance { get; private set; }

    public bool RoombaAchieved { get { return m_RoombaAchieved; } }
    private bool m_RoombaAchieved;

    public bool OilAchieved { get { return m_OilAchieved; } }
    private bool m_OilAchieved;

    public bool SOSAchieved { get { return m_SOSAchieved; } }
    private bool m_SOSAchieved;

    public bool SpacecraftReady { get { return RoombaAchieved && OilAchieved; } }

    [SerializeField] private Color m_TickColor;
    [SerializeField] private Image m_RoombaTick;
    [SerializeField] private Image m_OilTick;
    [SerializeField] private Image m_SOSTick;
    [SerializeField] private Image m_EscapeTick;

    private void Awake()
    {
        Instance = this;
    }

    public void AchieveRoomba()
    {
        m_RoombaAchieved = true;
        m_RoombaTick.color = m_TickColor;
    }

    public void AchieveOil()
    {
        m_OilAchieved = true;
        m_OilTick.color = m_TickColor;
    }

    public void AchieveSOS()
    {
        m_SOSAchieved = true;
        m_SOSTick.color = m_TickColor;
    }

    public void AchieveEscape()
    {
        m_EscapeTick.color = m_TickColor;
        GameManager.Instance.Victory();
    }
}