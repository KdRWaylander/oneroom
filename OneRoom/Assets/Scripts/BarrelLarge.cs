﻿using UnityEngine;

public class BarrelLarge : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 6f;

    private bool m_HasAlreadyBeenSearched;
    [SerializeField] private float m_ChanceThreshold = .1f;

    public void InteractWith(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance
            && m_HasAlreadyBeenSearched == false
            && player.GetComponentInChildren<ICollectible>() != null
            && player.GetComponentInChildren<ICollectible>().GameObject.transform.tag == "Weapon")
        {
            m_HasAlreadyBeenSearched = true;

            float chance = Random.value;
            if(chance >= 1 - m_ChanceThreshold)
            {
                player.GetComponentInChildren<Weapon>().AddAmmo(Random.Range(3, 5));
            }
        }
    }
}