﻿using UnityEngine;

public class EscapeComputer : MonoBehaviour, IInteractible
{
    public float InteractionDistance { get { return m_InteractionDistance; } }
    [SerializeField] private float m_InteractionDistance = 6f;

    public void InteractWith(Player player)
    {
        if (Vector3.Distance(player.transform.position, transform.position) < InteractionDistance && ObjectivesManager.Instance.SpacecraftReady)
        {
            ObjectivesManager.Instance.AchieveEscape();
        }
    }
}